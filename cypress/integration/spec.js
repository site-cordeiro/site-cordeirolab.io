/* eslint-disable no-undef */
/// <reference types="cypress" />

describe("Static site", () => {
  it("works", () => {
    cy.visit("/");
    cy.contains("h1", "Hello VuePress");
  });
});
