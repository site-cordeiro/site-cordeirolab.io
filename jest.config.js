const { resolve } = require('path');
const { compilerOptions } = require('./tsconfig.base.json');

module.exports = {
  rootDir: resolve(__dirname),
  testEnvironment: 'node',
  preset: 'ts-jest',
  globals: {
    'ts-jest': {
      tsconfig: {
        ...compilerOptions,
        sourceMap: true,
      },
    },
    __VERSION__: '',
    __DEV__: false,
    __SSR__: false,
  },
  moduleNameMapper: {
    '^@internal/(.*)$': `<rootDir>/__tests__/__fixtures__/$1`,
  },
  testMatch: ['<rootDir>/__tests__/**/*.spec.ts'],
  testPathIgnorePatterns: [
    '/node_modules/',
    '/__fixtures__/',
    '@vuepress/client',
  ],
  snapshotSerializers: [require.resolve('jest-serializer-vue')],

  // coverage config
  collectCoverageFrom: ['!**/*.d.ts'],
  coverageDirectory: 'coverage',
};
