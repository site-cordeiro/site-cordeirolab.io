import { defineClientAppEnhance } from "@vuepress/client";
//@ts-ignore
import Login from "./components/Login.vue";

// The app is the Vue application instance that created by createApp.
// The router is the Vue Router instance that created by createRouter.
// The siteData is an object that generated from user config, including base, lang, title, description, head and locales.

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.component("Login", Login);
});
