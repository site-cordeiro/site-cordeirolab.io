const locales = {
  // The key is the path for the locale to be nested under.
  // As a special case, the default locale can use '/' as its path.
  "/fr/": {
    lang: "fr-FR",
    title: "Titre du site",
    description: "Vue en français",
  },
  "/en/": {
    lang: "en-US",
    title: "Site title",
    description: "Vue in english",
  },
};

export default locales;
