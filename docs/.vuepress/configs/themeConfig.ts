import { navbar } from "./index";

const isProd = process.env.NODE_ENV === "production";

const themeConfig = {
  logo: "https://vuejs.org/images/logo.png",
  repo: "https://gitlab.com/site-cordeiro/site-cordeiro.gitlab.io",
  docsDir: "docs",
  docsRepo: "https://gitlab.com/site-cordeiro/site-cordeiro.gitlab.io",
  docsBranch: "main",
  editLinkPattern: ":repo/-/edit/:branch/:path",
  // theme-level locales config
  locales: {
    "/fr/": {
      // navbar
      navbar: navbar.fr,
      selectLanguageName: "Français",
      selectLanguageText: "Languages",
      selectLanguageAriaLabel: "Languages",
      // sidebar
      // sidebar: sidebar.fr,
      // page meta
      editLinkText: "Editer cette page",
      // 404 page
      notFound: ["erreur 404"],
      backToHome: "retour à l'acceuil",
      // custom containers
      tip: "提示",
      warning: "注意",
      danger: "警告",
      lastUpdatedText: "上次更新",
      contributorsText: "贡献者",
      // a11y
      openInNewWindow: "在新窗口打开",
      toggleDarkMode: "切换夜间模式",
    },
    /**
     * English locale config
     */
    "/en/": {
      // navbar
      navbar: navbar.en,
      selectLanguageName: "English",
      selectLanguageText: "Languages",
      selectLanguageAriaLabel: "Languages",
      // sidebar
      //  sidebar: sidebar.en,
      // page meta
      editLinkText: "Edit this page",
      lastUpdatedText: "上次更新",
      contributorsText: "贡献者",
      // custom containers
      tip: "提示",
      warning: "注意",
      danger: "警告",
      // 404 page
      notFound: ["error 404 not found"],
      backToHome: "back to Home",
      // a11y
      openInNewWindow: "在新窗口打开",
      toggleDarkMode: "切换夜间模式",
    },
  },
  themePlugins: {
    // only enable git plugin in production mode
    git: isProd,
  },
};

export default themeConfig;
