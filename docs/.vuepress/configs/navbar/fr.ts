import type { NavbarConfig } from "@vuepress/theme-default";
export const fr: NavbarConfig = [
  // NavbarItem
  {
    text: "Test",
    link: "/test/test.md",
  },
  // NavbarGroup
  {
    text: "Guide",
    children: [
      "/guide/page.md",
      "/guide/getting-started.md",
      {
        text: "SubGroup",
        children: ["/group/sub/foo.md", "/group/sub/bar.md"],
      },
    ],
  },
  {
    text: "Group 2",
    children: [
      {
        text: "Always active",
        link: "/",
        // this item will always be active
        activeMatch: "/",
      },
      {
        text: "Active on /guide/",
        link: "/guide/",
        // this item will be active when current route path starts with /foo/
        // regular expression is supported
        activeMatch: "^/guide/",
      },
    ],
  },
  // string - page file path
];
