// import { defineUserConfig } from 'vuepress'
// import type { DefaultThemeOptions } from 'vuepress'
// import { path } from '@vuepress/utils'

// import locales from './config/locales'
// import themeConfig from './config/themeConfig'

// export default defineUserConfig<DefaultThemeOptions>({
//   locales,
//   themeConfig,
//   head: [['link', { rel: 'icon', href: 'https://vuejs.org/images/logo.png' }]],

// })

import { defineUserConfig } from '@vuepress/cli';
import type { DefaultThemeOptions } from '@vuepress/theme-default';
// import { path } from "@vuepress/utils";
import locales from './configs/locales';
import themeConfig from './configs/themeConfig';

// const isProd = process.env.NODE_ENV === 'production'

export default defineUserConfig<DefaultThemeOptions>({
  base: '/',
  // site-level locales config
  locales,
  themeConfig,
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: `/images/favicon.ico`,
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: `/images/favicon.ico`,
      },
    ],
    ['link', { rel: 'manifest', href: '/manifest.webmanifest' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'application-name', content: 'VuePress' }],
    ['meta', { name: 'apple-mobile-web-app-title', content: 'VuePress' }],
    [
      'meta',
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],

    // [
    //   "link",
    //   { rel: "apple-touch-icon", href: `/images/icons/apple-touch-icon.png` },
    // ],
    // [
    //   "link",
    //   {
    //     rel: "mask-icon",
    //     href: "/images/icons/safari-pinned-tab.svg",
    //     color: "#3eaf7c",
    //   },
    // ],

    ['meta', { name: 'msapplication-TileColor', content: '#3eaf7c' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
  ],

  // markdown: {
  //   importCode: {
  //     handleImportPath: (str) =>
  //       str.replace(
  //         /^@vuepress/,
  //         path.resolve(__dirname, "../../packages/@vuepress")
  //       ),
  //   },
  // },

  //   alias: {
  //     '@alias': path.resolve(__dirname, './path/to/some/dir'),
  //   },

  plugins: [
    // ['@vuepress/plugin-debug'],
    // [
    //   '@vuepress/plugin-docsearch',
    //   {
    //     apiKey: '3a539aab83105f01761a137c61004d85',
    //     indexName: 'vuepress',
    //     searchParameters: {
    //       facetFilters: ['tags:v2'],
    //     },
    //     locales: {
    //       '/zh/': {
    //         placeholder: '搜索文档',
    //       },
    //     },
    //   },
    // ],
    // [
    //   '@vuepress/plugin-google-analytics',
    //   {
    //     // we have multiple deployments, which would use different id
    //     id: process.env.DOCS_GA_ID,
    //   },
    // ],
    ['@vuepress/plugin-pwa'],
    // [
    //   '@vuepress/plugin-pwa-popup',
    //   {
    //     locales: {
    //       '/fr/': {
    //         message: 'Nouveau contenu disponible.',
    //         buttonText: 'Refresh',
    //       },
    //       '/en/': {
    //         message: 'New content is available.',
    //         buttonText: 'Refresh',
    //       },
    //     },
    //   },
    // ],
    // [
    //   '@vuepress/plugin-register-components',
    //   {
    //     componentsDir: path.resolve(__dirname, './components'),
    //   },
    // ],
  ],
});
