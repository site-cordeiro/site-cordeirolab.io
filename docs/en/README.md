---
home: true
heroImage: /images/home.jpg
heroText: Hero Title
tagline: Hero subtitle
actions:
  - text: Introduction
    link: /en/guide/
    type: primary
  - text: Get Started
    link: /en/guide/getting-started.html
    type: secondary
features:
  - title: Programmation
    details: JavaScript, Rust, ect
  - title: CI/CD
    details: DevOps, GitLab, ...
  - title: Autre...
    details: blablabla, ...
footer: MIT Licensed | Copyright © 2021 Maxime Cordeiro
---
