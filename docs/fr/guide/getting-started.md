---
lang: fr-FR
title: Guide
description: page guide
prev:
  text: guide
  link: /fr/guide
---

# Hello Getting Started

[Home](../README.md)

```ts{1,6-8}
import type { UserConfig } from '@vuepress/cli'

export const config: UserConfig = {
  title: 'Hello, VuePress',

  themeConfig: {
    logo: 'https://vuejs.org/images/logo.png',
  },
}
```

```ts
// line-numbers is enabled by default
const line2 = "This is line 2";
const line3 = "This is line 3";
```

```ts:no-line-numbers
// line-numbers is disabled
const line2 = 'This is line 2'
const line3 = 'This is line 3'
```

```md
<!-- This will be kept as is by default -->

1 + 2 + 3 = {{ 1 + 2 + 3 }}
```

```md:no-v-pre
<!-- This will be compiled by Vue -->
1 + 2 + 3 = {{ 1 + 2 + 3 }}
```

```js:no-v-pre
// This won't be compiled correctly because of js syntax highlighting
const onePlusTwoPlusThree = {{ 1 + 2 + 3 }}
```

test foo
@[code js](./foo.js)

One plus one equals: {{ 1 + 1 }}

<span v-for="i in 3"> span: {{ i }} </span>

This is default theme built-in `<Badge />` component <Badge text="demo" />
