---
lang: fr-FR
title: Guide
description: page guide
prev:
  text: guide
  link: /fr/guide
next:
  text: Get Started
  link: /fr/guide/getting-started.html
---

# Hello page

<!-- relative path -->

[Home](../README.md)  
[Getting Started](./getting-started.md)

<!-- absolute path -->

[Guide](/fr/guide/README.md)

<!-- URL -->

[GitHub](https://github.com)
